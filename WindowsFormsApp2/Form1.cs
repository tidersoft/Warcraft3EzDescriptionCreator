﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;

namespace WindowsFormsApp2
{

   
    public partial class Form1 : Form
    {

        bool noty = true;

        private const String APP_ID = "Warcraft 3 Ez Description Creator";
        public Form1()
        {
            InitializeComponent();
        }

        private void Toast() {
            // Get a toast XML template
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastImageAndText04);
            XmlNodeList stringElements = toastXml.GetElementsByTagName("text");
            
                stringElements[0].AppendChild(toastXml.CreateTextNode("Warcraft 3 Ez Descriptin Creator"));
                stringElements[1].AppendChild(toastXml.CreateTextNode("The Source Code has been copied to the clipboard."));
                stringElements[2].AppendChild(toastXml.CreateTextNode("Now you can paste it into the editor."));

            // Specify the absolute path to an image
            String imagePath = "file:///" + Path.GetFullPath("toastImageAndText.png");
            XmlNodeList imageElements = toastXml.GetElementsByTagName("image");
            imageElements[0].Attributes.GetNamedItem("src").NodeValue = imagePath;


            ToastNotification toast = new ToastNotification(toastXml);

            ToastNotificationManager.CreateToastNotifier(APP_ID).Show(toast);
        }

        private void ParseCode() {
            int selectPos = SourceBox.SelectionStart;
            SourceBox.Text = SourceBox.Text.Replace("\n", "");
       
            String text = SourceBox.Text;
            string colorcode = "FFFFFFFF";
            int argb = Int32.Parse(colorcode, NumberStyles.HexNumber);
            Color color = Color.FromArgb(argb);
            int count = 0;
            int recu = 0;
            EditBox.Text = "";
            if(toolStrip2.Items!=null)
            for (int i=0;i<  toolStrip2.Items.Count;i++) {
                toolStrip2.Items.Remove(toolStrip2.Items[i]);
            }
            for (int i = 0; i < text.Length; i++) {
                if (text[i].Equals('|'))
                {
                    if (i < text.Length - 1)
                    {
                        if (text[i + 1].Equals('c') && text.Length > i+9)
                        {
                            colorcode = text.Substring(i + 2, 8);
                            try
                            {
                                argb = Int32.Parse(colorcode, NumberStyles.HexNumber);

                                color = Color.FromArgb(argb);
                                count = i - recu;
                                recu += 10;

                                i += 9;
                            }
                            catch ( System.FormatException e) {


                            }
                                // EditBox.SelectionStart = i;
                           
                        }

                        else if (text[i + 1].Equals('r'))
                        {
                            int end = EditBox.SelectionStart;
                            //  EditBox.AppendText( text.Substring(text.Length-(i-8),i-10 ));
                            EditBox.Select(count, end);
                            EditBox.SelectionColor = color;
                            AddColorButons(color);
                            EditBox.Select(end, 0);
                            EditBox.SelectionColor = Color.White;
                            recu += 2;
                            i += 1;

                              ///ds
                        }
                        else if (text[i + 1].Equals('n')) {
                            EditBox.AppendText(Environment.NewLine);
                            recu += 1;
                            i++;
                        }
                    }
                }
                else {
                    EditBox.AppendText(text[i].ToString());

                }
            }
            SourceBox.SelectionStart = selectPos;
        }

        private void AddColorButons(Color color) {
            for (int i = 0; i < toolStrip2.Items.Count; i++)
            {
                if (toolStrip2.Items[i].BackColor.Equals(color))
                    return;
            }
            ToolStripButton Button = new ToolStripButton();
            Button.AutoSize = false;
            Button.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            //Button.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            Button.ImageTransparentColor = System.Drawing.Color.Magenta;
           Button.Name = formatColor(color);
           Button.Size = new System.Drawing.Size(75, 35);
          // Button.Text = "";
            Button.BackColor = color;
           Button.ToolTipText = formatColor(color);
            Button.Click += new System.EventHandler(this.toolStripButton1_Click);
            this.toolStrip2.Items.Add(Button);


        }

        private void OpenColorTag() {
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                AddColorTag(colorDialog.Color);
            }
        }

        private string formatColor(Color color) {
            return "|cFF" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }

        private void AddColorTag(Color color) {
            int start = SourceBox.SelectionStart;

            SourceBox.Text = SourceBox.Text.Insert(start, formatColor(color));
            SourceBox.SelectionStart = start + 10;
        }

        private void CloseColorTag() {
            int start = SourceBox.SelectionStart;
            SourceBox.Text = SourceBox.Text.Insert(start, "|r");
            SourceBox.SelectionStart = start + 2;

        }

        private void EditBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.C && e.Shift && e.Control)
            {
                OpenColorTag();
            }
            else if (e.KeyCode == Keys.R && e.Shift && e.Control)
            {
                CloseColorTag();

            }
            else if (e.KeyCode == Keys.Enter)
            {
                int start = SourceBox.SelectionStart;
                SourceBox.Text = SourceBox.Text.Insert(start, "|n");
                SourceBox.SelectionStart = start + 1;

            }
            else if (e.KeyCode == Keys.B && e.Shift && e.Control) {
                copyToClippboard();
            }
           
        }

        private void SourceBox_TextChanged(object sender, EventArgs e)
        {
            ParseCode();
        }

       

        private void SaveFile() {

            SaveFileDialog savefile = new SaveFileDialog();
            // set a default file name
            savefile.FileName = "unknown.txt";
            // set filters - this can be done in properties as well
            savefile.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (savefile.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(savefile.FileName))
                    sw.WriteLine(SourceBox.Text);
            }
        }

        private void LoadFile() {
            StreamReader myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamReader sw = new StreamReader(openFileDialog1.FileName))
                    {

                        if (sw != null)
                        {
                            SourceBox.Text=sw.ReadLine();
                            ParseCode();
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }

        private void loadFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadFile();
        }


        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SourceBox.Text = "";
            ParseCode();
        }

        private void addColorTagCtrShiftCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenColorTag();
        }

        private void closeColorTagToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CloseColorTag();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AddColorTag(((ToolStripButton)sender).BackColor);
        }

        private void SourceBox_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }

        private void copyToClippboard() {
            int start = SourceBox.SelectionStart;
            SourceBox.SelectAll();
            SourceBox.Copy();
            SourceBox.DeselectAll();
            SourceBox.SelectionStart = start;
            if(noty)
            Toast();
        }

        private void copyToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyToClippboard();
        }

        private void copyToClipboardCtrShiftBToolStripMenuItem_Click(object sender, EventArgs e)
        {
            copyToClippboard();
        }

        private void toolStripButton19_Click(object sender, EventArgs e)
        {
            if (noty)
            {
                noty = false;
                notyButton.Text = "Off";
            }
            else {
                noty = true;
                notyButton.Text = "On";
            }
        }
    }
}
